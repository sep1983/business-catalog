import React, { Component } from 'react'
import {Query} from "react-apollo";
import { GET_CATEGORY } from "./queries/RepoCategoriesQuery";
import {ArrowDown} from "react-feather";

export default class SelectCategory extends Component {

    constructor(props) {
        super(props);
        this._handleChange = this._handleChange.bind(this);
        this.state = {
            category: ''
        }
    }

    _handleChange(event){
        this.setState({ category: event.target.value });
        const index = event.nativeEvent.target.selectedIndex;
        const title = event.nativeEvent.target[index].text;
        this.props.changeSelect(event.target.value, title);
    }

    render() {
        const { category } = this.state;
        return (
            <Query query={GET_CATEGORY}>
                {({loading, error, data}) => {
                    if (loading) return <div>Fetching</div>
                    if (error) return <div>Error</div>
                    const AllCategories = data.allCategories.edges;
                    const color = AllCategories.map(function (c, index) {
                        return (<option key={index} value={c.node.id}>{c.node.name}</option>)
                    });
                    return (
                        <div className="relative nb1 fl w-100">
                            <select
                                className="input-reset br4
                                                    b--black-30 ba pa2
                                                    bg-transparent
                                                    z-1 relative
                                                    mb2 db w-100"
                                value={category}
                                required
                                onChange={this._handleChange}>
                                <option></option>
                                {color}
                            </select>
                            <button type="button"
                                    className="z-0 w-auto
                                                    absolute top-0 right-0
                                                    black-80
                                                    bg-transparent f5 pv2 ph3 bn bl-l b--black-30
                                                    bg-hover-mid-gray">
                                <ArrowDown size={12} strokeWidth={3}/>
                            </button>
                        </div>
                    )
                }}
            </Query>
        );
    }
}
