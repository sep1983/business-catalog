import React, { Component } from 'react'
import Search   from './Search'
import Login, { NewUser }    from './Login'
import Logout   from './Logout'
import { Link } from 'react-router-dom'
import { AUTH_TOKEN } from '../constants'

class App extends Component {

    constructor() {
        super();
        this.state = { 
            authToken : localStorage.getItem(AUTH_TOKEN),
            loginForm: true
        };
    }

    onChildChanged() {
      this.setState({ authToken : localStorage.getItem(AUTH_TOKEN) });
    }

    onLogSuccess(){
       this.props.history.push('/list');
    }

    render() {

        const { authToken, loginForm } = this.state;

        return (
        <div>

            <main className="cf pa2">
                <div className="fl w-50-ns w-100 pa2">

                    <div className="fl w-100-ns w-50-ns ph2">
                        <a href="" className="pv2 grow db no-underline black">
                            <img className="db w-100" src="https://s3-us-west-2.amazonaws.com/prnt/hw090911_960.jpg"/>
                        </a>
                        <a href="" className="pv2 grow db no-underline black">
                            <img className="db w-100" src="https://s3-us-west-2.amazonaws.com/prnt/hw-080411-cargo_960.jpg"/>
                        </a>

                        { authToken ? (
                            <nav className="flex bg-red justify-between bb b--white-10">
                                <Logout callbackParent={() => this.onChildChanged() } />
                                <div className="flex-grow pa3 flex items-center">
                                    <Link className="f6 link dib white dim mr3 mr4-ns" to="list">Editar</Link>
                                </div>
                            </nav>
                        ) : (
                        <div>
                            <div className="flex">
                                <div className="w-50 pa2">
                                  <a
                                    onClick={e => this.setState({ loginForm: true })}
                                    className="f4 link dim gray underline-hover">Ingresar</a>
                                </div>
                                <div className="w-50 pa2">
                                  <a 
                                    onClick={e => this.setState({ loginForm: false })}
                                    className="f4 link dim gray underline-hover">Nuevo</a>
                                </div>
                            </div>
                            { loginForm ? (
                                <Login callbackParent= { () => this.onLogSuccess() }/>        
                            ) : (
                                <NewUser/>        
                            )}                            
                        </div>
                            
                        )}

                        <a href="" className="pv2 grow db no-underline black">
                            <img className="db w-100" src="https://s3-us-west-2.amazonaws.com/prnt/hw-residency-cargo_960.jpg"/>
                        </a>
                        <a href="" className="pv2 grow db no-underline black">
                            <img className="db w-100" src="https://s3-us-west-2.amazonaws.com/prnt/orchid-2-mnkr_960.jpg"/>
                        </a>
                        <a href="" className="pv2 grow db no-underline black">
                            <img className="db w-100" src="https://s3-us-west-2.amazonaws.com/prnt/O270711_960-2.jpg"/>
                        </a>
                        <a href="" className="pv2 grow db no-underline black">
                            <img className="db w-100" src="https://s3-us-west-2.amazonaws.com/prnt/adam-stern-020510_960.jpg"/>
                        </a>
                        <a href="" className="pv2 grow db no-underline black">
                            <img className="db w-100" src="https://s3-us-west-2.amazonaws.com/prnt/adam-stern-130610_960.jpg"/>
                        </a>
                        <a href="" className="pv2 grow db no-underline black">
                            <img className="db w-100" src="https://s3-us-west-2.amazonaws.com/prnt/adam-stern-031209_960-2.jpg"/>
                        </a>
                    </div>

                    <div className="fl w-100 w-50-ns ph2">
                        <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                                                                                   src="https://s3-us-west-2.amazonaws.com/prnt/adam-stern-191110_960.jpg"/></a>
                        <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                                                                                   src="https://s3-us-west-2.amazonaws.com/prnt/zh170311.4.cargo_960.jpg"/></a>
                        <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                                                                                   src="https://s3-us-west-2.amazonaws.com/prnt/hwspringtour-cargo_960-1.jpg"/></a>
                        <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                                                                                   src="https://s3-us-west-2.amazonaws.com/prnt/cc-shanee_960.jpg"/></a>
                        <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                                                                                   src="https://s3-us-west-2.amazonaws.com/prnt/zach-hurd-101218_960.jpg"/></a>
                        <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                                                                                   src="https://s3-us-west-2.amazonaws.com/prnt/hw170211pie-cargo_960.jpg"/></a>
                        <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                                                                                   src="https://s3-us-west-2.amazonaws.com/prnt/ZachHurd-190111s_960.jpg"/></a>
                    </div>

                </div>

              <div className="fl w-100 w-50-ns ph2">
                  <a href="" className="no-underline pv2 grow db"><img className="db w-100"
                  src="https://s3-us-west-2.amazonaws.com/prnt/elevaters.jpg"/></a>
                  <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                  src="https://s3-us-west-2.amazonaws.com/prnt/elevaters030211_960.jpg"/></a>
                  <a href="#" className="no-underline pv2 grow db"><img className="db w-100"
                  src="https://s3-us-west-2.amazonaws.com/prnt/pink-and-noseworthy-22.12.10-cargo_960.jpg"/></a>
                  <a href="" className="pv2 grow db no-underline black"><img className="db w-100"
                  src="https://s3-us-west-2.amazonaws.com/prnt/hw18-240112-cc_960.jpg"/></a>
              </div>

            </main>
        </div>
  )
  }
}

export default App
