import React, { Component } from 'react'
import { Query, Mutation } from 'react-apollo'
import SelectCategory from "../CommonQuerys"
import GET_CATEGORY, { CHANGECATEG, DELETECATEG } from '../queries/RepoCategoriesQuery'
import {ArrowDown, Save, Trash} from "react-feather";

class NewItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showMessage: false,
            category: '',
            title:'',
            message:{
                color:' ',
                text:' '
            }
        };
        this.validateForm1 = this.validateForm1.bind(this);
        this.validateForm2 = this.validateForm2.bind(this);
        this.handleChangeCategory = this.handleChangeCategory.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
    }

    validateForm1(){
        if(this.state.category){
            return false;
        }
        return true;
    }

    validateForm2(){
        if(this.state.title.length > 2){
            return false;
        }
        return true;
    }

    handleChangeCategory(categoryId, categoryTitle) {
        this.setState({
            category: categoryId,
            title: categoryTitle
        });
    }

    handleChangeTitle(event) {
        this.setState({title: event.target.value});
    }

    handleChangeMessage(data) {
        this.setState(data);
    }

    render() {
        const { category, title } = this.state;

        return (
            <div className="pa4 black-80">
                <legend className="f4 fw6 ph0 mh0">Editar Categoria</legend>
                <div className="mt3">
                    <label className="f6 b db mb2">Categoria </label>
                    <SelectCategory changeSelect={this.handleChangeCategory}/>

                    <label htmlFor="name" className="f6 b db mb2">Nuevo Nombre <span className="red">*</span></label>
                    <input
                    className="input-reset bg-transparent b--black-30 ba pa2 mb2 db br4 w-100"
                    name="title"
                    type="text"
                    placeholder=""
                    value={title}
                    onChange={this.handleChangeTitle}
                    required/>

                    <Mutation
                        mutation={CHANGECATEG}
                        variables={{ name:title, id: category }}

                        refetchQueries={(color) => {
                            return [{ query:GET_CATEGORY }];
                        }}

                        onCompleted={() => {
                            let result = {
                                category: '',
                                title: ''
                            };

                            this.handleChangeMessage(result);

                            this.props.message({
                                text: 'Se cambio el registro',
                                color:'success' });
                        }}

                        onError={(data) => {
                            let result = {
                                category: '',
                                title:'',
                            };

                            this.handleChangeMessage(result);

                            this.props.message({
                                text: data.message + ' , error_5239',
                                color:'error'});

                        }}
                    >
                        {(mutation, { loading, error }) => {

                            const butt = this.validateForm2() ? ' light-gray ' : '  bg-animate hover-bg-silver black ';
                            const classes = "child pointer ph3 pv2 br4 mb2  " + butt +
                                " bg-moon-gray";

                            return (
                                <div className="tc ma4">
                                    <a onClick  = {this.validateForm2()? (e) =>e.preventDefault() : mutation}
                                       className= {classes}>
                                        <div  className="dib v-mid">
                                            <div title="Guardar">
                                                <Save size={14}/>
                                            </div>
                                        </div>
                                        <div className="dib ml1"> Guardar </div>
                                    </a>
                                </div>
                            )

                        }}
                    </Mutation>

                    <Mutation
                     mutation={DELETECATEG}
                     variables={{id: category}}
                     refetchQueries={(color) => {
                         return [{ query:GET_CATEGORY }];
                     }}
                     onCompleted={(data) => {
                         let result = {
                             category: '',
                             title:'',
                         };

                         if(data.deleteCategory.ok == false){
                             this.props.message({
                                 text: data.deleteCategory.message,
                                 color:'error' })
                         } else {
                             this.props.message({
                                 text: 'Se elimino el registro',
                                 color:'success' })
                         }

                         this.handleChangeMessage(result);
                     }}

                     onError={(data) => {
                         this.props.message({
                             text: data.message + ' , error_5241',
                             color:'error' });
                     }}
                     >
                     {(mutation) => {
                         const butt = this.validateForm1() ? ' light-gray ' : '  bg-animate hover-bg-silver black ';
                         const classes = "child pointer ph3 pv2 br4 " + butt +
                             " bg-moon-gray";

                         return (
                             <div className="tc ma4">
                                 <a onClick  = {this.validateForm1()?  (e) =>e.preventDefault() : mutation}
                                    className= {classes}>
                                     <div  className="dib v-mid">
                                         <div title="Borrar">
                                             <Trash size={14}/>
                                         </div>
                                     </div>
                                     <div className="dib ml1"> Borrar </div>
                                 </a>
                             </div>
                         )
                     }}
                    </Mutation>
                </div>
            </div>
        )
    }
}

export default NewItem
