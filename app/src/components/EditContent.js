import React, { Component, PureComponent } from 'react'
import Logout  from './Logout'
import NewItem from './NewItem'
import ListItemsToEdit from './ListItemsToEdit'
import Message from './Message'
import NewCateg from './NewCateg'
import EditCateg from './EditCategory'
import NavBar from './NavBar'
import {ArrowUp, XCircle} from "react-feather";

class EditContent extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            alert: {},
            showItem: 'dn',
            showEdiCat: 'dn',
            showNewCat: 'dn',
            scroll: false,
        };

        this.newMessage = this.newMessage.bind(this);
        this.hideComponent = this.hideComponent.bind(this);
    }

    isEmpty(obj) {
        for(let key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    newMessage(data){
        if(!this.isEmpty(data) || this.state.alert !== data){
            this.setState({alert: data});
        }
    }

    isInTop (){
        let doc = document.documentElement;
        let tmpScroll = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
        return tmpScroll;
    }


    scrollUp () {
        let top =this.isInTop();
        if (top > 0) {
            window.scrollTo(0, top - 10);
            setTimeout(()=>this.scrollUp(), 10);
        }
    }

    hideComponent(name) {
        this.setState({
            showItem: 'dn',
            showEdiCat: 'dn',
            showNewCat: 'dn'
        });
        switch (name) {
            case "showItem":
                this.setState({ showItem: '' });
                this.scrollUp()
                break;
            case "showEdiCat":
                this.setState({ showEdiCat: '' });
                this.scrollUp()
                break;
            case "showNewCat":
                this.setState({ showNewCat: '' });
                this.scrollUp()
                break;
        }
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => {
                this.setState({scroll: this.isInTop() > 0 ? true : false})
            },
            2000
        );
    }

    render() {
        const { showItem, showNewCat, showEdiCat, scroll, alert } = this.state;

        const classMenu = "bg-near-white br3 w-20-l w-40-m w-80 " +
            "mt5-ns mt4-m absolute mt1 left-1 right-1 z-2 ";

        return (
                <div className="avenir">

                    <NavBar click={this.hideComponent}/>
                    <div className="h3-ns mt4 h2"></div>

                    <div className={classMenu + showItem}>
                        <div className="pa2 fr" onClick={() => this.hideComponent()}><XCircle/></div>
                        <NewItem   message={this.newMessage}/>
                    </div>

                    <div className={classMenu + showNewCat}>
                        <div className="pa2 fr" onClick={() => this.hideComponent()}><XCircle/></div>
                        <NewCateg  message={this.newMessage}/>
                    </div>

                    <div className={classMenu + showEdiCat}>
                        <div className="pa2 fr" onClick={() => this.hideComponent()}><XCircle/></div>
                        <EditCateg message={this.newMessage}/>
                    </div>

                    <div className="z-1 ma2">
                        <ListItemsToEdit />
                    </div>

                    <div className="">
                        <Message data={alert} />
                    </div>

                    { scroll ?
                        <div className="pa1 bg-light-gray fixed  bw1 light-gray br-100 bottom-2  right-2">
                            <div className="pa2 bg-moon-gray    bw1 br-100  grow-large  ">
                              <a onClick={() =>this.scrollUp()}>
                                <ArrowUp/>
                              </a>
                            </div>
                        </div>
                        : null
                    }

                </div>
        )
  }
}

export default EditContent
