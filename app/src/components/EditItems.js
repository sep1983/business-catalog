import React, { Component } from 'react';
import { Query, Mutation } from 'react-apollo';
import RowEditItems from './RowEditItems';
import { LIST_ITEMS } from '../queries/ItemQuery';


class EditItems extends Component {

    constructor(props) {
        super(props);       
        this.state = {};
    }

    render() {

        const classtd = "pv2 pr3 bb b--black-20 v-top";
        const classhd = "fw6 tl pa3 bg-white";

        const {category, title, price, quantity,
            description, fileLogo} = this.state;

        return (
                <div className="overflow-auto">
                    <table className="f6 w-100 center" cellSpacing="0">
                        <thead onClick={this.handleChangeCategory}>
                            <tr className="stripe-dark">
                                <th className={classhd}>Id</th>
                                <th className={classhd}>Inactivo</th>
                                <th className={classhd}>Categoria</th>
                                <th className={classhd}>Titulo</th>
                                <th className={classhd}>Precion Unitario</th>
                                <th className={classhd}>Cantidad</th>
                                <th className={classhd}>Descripción</th>
                                <th className={classhd}>Imagen</th>
                                <th className={classhd}>Acción</th>
                            </tr>
                        </thead>
                        <tbody className="lh-copy">
                            <Query query={LIST_ITEMS}>
                                {({ loading, error, data }) => {
                                    if (loading) return <tr><td>Fetching</td></tr>
                                    if (error) return <tr><td>Error</td></tr>

                                    const AllItems = data.allItems.edges;

                                    const color = AllItems.map(function (item, index) { 
                                        let s = item.node;
                                        let idCat = s.category.id;
                                        let id = atob(s.id).slice(6);
                                        return (<RowEditItems 
                                                    key={index}
                                                    iditem={id}
                                                    iditemRow={s.id}
                                                    idCateg={idCat}
                                                    inactive={s.noActive}
                                                    title={s.title}
                                                    unitPrice={s.unitPrice}
                                                    quantity={s.quantity}
                                                    descrip={s.description}
                                                    file={s.thumbnail}/> 
                                               )
                                    }, this);
                                    return ([...color])                                
                                }}                                
                            </Query>  
                        </tbody>
                    </table>
                </div>
                )
    }
}

export default EditItems
