import React, { Component } from 'react';
import { LIST_ITEMS } from '../queries/ItemQuery';
import {Query} from "react-apollo";
import {Edit, Trash, Tag} from "react-feather";
import StackGrid,  { transitions, easings } from "react-stack-grid";

const transition = transitions.scaleDown;

export default class ListItemsToEdit extends Component {
    render(){
        return(
            <Query query={LIST_ITEMS}>
                {({ loading, error, data }) => {
                    if (loading) return <tr><td>Fetching</td></tr>
                    if (error) return <tr><td>Error</td></tr>

                    const AllItems = data.allItems.edges;

                    const color = AllItems.map(function (item, index) {
                        let s = item.node;
                        let id = atob(s.id).slice(6);
                        const classes = "child pointer ph3 pv2 br4   bg-animate" +
                            " hover-bg-silver black   bg-moon-gray";

                        return (
                            <figure key={index} className="db ma0 w-100 hide-child">
                                <img src={s.thumbnail} className="h-auto w-100 br4 v-top" title={s.title}/>
                                <span className="white absolute left-0 top-0 v-mid w-100 tc h-100 child br4 bg-black-40">
                                    <div className="bg-white-70 b br4 mv3 mh2 pa2 w-90 black inline-flex items-center">
                                        <div className="dib ml3"><Tag size={16}/></div>
                                        <span className="dib ml1 f6 ttc v-mid truncate"> {s.title} </span>
                                    </div>
                                    <div className="tc">
                                        <a
                                            className= {classes}>
                                            <div  className="dib v-mid">
                                                <div title="Editar">
                                                    <Edit size={14}/>
                                                </div>
                                            </div>
                                            <div className="dib-ns ml1 dn"> Editar </div>
                                        </a>
                                    </div>
                                </span>
                            </figure>
                        )
                    }, this);

                    return (
                            <StackGrid
                                monitorImagesLoaded
                                columnWidth={230}
                                duration={600}
                                gutterWidth={15}
                                gutterHeight={15}
                                easing={easings.cubicOut}
                                appearDelay={60}
                                appear={transition.appear}
                                appeared={transition.appeared}
                                enter={transition.enter}
                                entered={transition.entered}
                                leaved={transition.leaved} >
                                {[...color]}
                            </StackGrid>)
                }}
            </Query>
        );
    }
}
