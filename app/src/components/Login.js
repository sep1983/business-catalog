import React, { Component } from 'react'
import { Mutation } from "react-apollo";
import { Redirect } from 'react-router';
import { AUTH_TOKEN } from '../constants'
import LOGIN, { CREATEUSER } from '../queries/UsersQuery';


class Login extends Component{

    constructor(props) {
        super(props);

        this.state = {
            email: 'charles1003@mi.co',
            passwd: '1003',
            login: false
        };

        this._confirm = this._confirm.bind(this);
        this._saveUserData = this._saveUserData.bind(this);

    };


    _confirm = async data => {
        const { authToken } = data.login
        this._saveUserData(authToken)
        this.setState({ login : true })
        this.props.callbackParent();
    }


     _saveUserData = token => {
        localStorage.setItem(AUTH_TOKEN, token)
    }


    render(){

        const { email, passwd, redirect } = this.state;

        return redirect ? <Redirect to="/EditContent" /> : (
            <main className="pa1 black-80">
                <form className="measure center">
                    <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                        <div className="mt3">
                            <label className="db fw6 lh-copy f6" htmlFor="email-address">Correo</label>
                            <input
                                value={email}
                                onChange={e => this.setState({ email: e.target.value })}
                                className="pa2 input-reset ba w-100"
                                type="email"
                                name="email"
                                id="email"/>
                        </div>

                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password">Clave</label>
                            <input
                                value={passwd}
                                onChange={e => this.setState({ passwd: e.target.value })}
                                className="b pa2 input-reset ba w-100"
                                type="password"
                                name="passwd"
                                id="passwd"/>
                        </div>

                    </fieldset>
                    <div className="input-field ">
                        <Mutation
                            mutation={LOGIN}
                            variables={{ email, passwd }}
                            onCompleted={data => this._confirm(data)}
                        >
                            {mutation => (
                                <div
                                    onClick={mutation}
                                    className="b ph3 pv2 enviar input-reset bnt-search grow pointer f6 dib">
                                    Enviar
                                </div>
                            )}
                        </Mutation>
                    </div>
                </form>

            </main>
        )
    }
}


class NewUser extends Component{

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            passwd: '',
            repPasswd: ''
        };

        this._confirm = this._confirm.bind(this);
        this._saveUserData = this._saveUserData.bind(this);

    };


    _confirm = async data => {
        const { authToken } = data.login
        this._saveUserData(authToken)
        this.setState({ login : true })
        this.props.callbackParent();
    }


     _saveUserData = token => {
        localStorage.setItem(AUTH_TOKEN, token)
    }


    render(){

        const classBase = "w-90 ba br2 pa3 alert";
        const { email, passwd, repPasswd } = this.state;

        return (
            <main className="pa1 black-80">
                <form className="measure center">
                    <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                        
                        <div className="mt3">
                            <label className="db fw6 lh-copy f6" htmlFor="email-address">Correo</label>
                            <input
                                value={email}
                                onChange={e => this.setState({ email: e.target.value })}
                                className="pa2 input-reset ba w-100"
                                type="email"
                                name="email"
                                id="email"/>
                        </div>

                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password">Clave</label>
                            <input
                                value={passwd}
                                onChange={e => this.setState({ passwd: e.target.value })}
                                className="b pa2 input-reset ba w-100"
                                type="password"
                                name="passwd"
                                id="passwd"/>
                        </div>

                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password">Repetir Clave</label>
                            <input
                                value={passwd}
                                onChange={e => this.setState({ repPasswd: e.target.value })}
                                className="b pa2 input-reset ba w-100"
                                type="password"
                                name="repPasswd"
                                id="passwd"/>
                        </div>
                    </fieldset>
                    <div className="input-field ">
                        <Mutation
                            mutation={CREATEUSER}
                            variables={{ email, passwd }}
                            onCompleted={data => this._confirm(data)}
                        >
                            {mutation => (
                                <div
                                    onClick={mutation}
                                    className="b ph3 pv2 enviar input-reset bnt-search grow pointer f6 dib">
                                    Enviar
                                </div>
                            )}
                        </Mutation>
                    </div>
                </form>

            </main>
        )
    }
}

export { Login  as default, Login, NewUser };
