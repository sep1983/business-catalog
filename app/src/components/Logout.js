import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { withRouter } from "react-router"
import { AUTH_TOKEN } from '../constants'
import {LogOut, User, Settings, ChevronDown, ChevronUp} from "react-feather";

class Logout extends Component{

    constructor(props) {
        super(props)
        this.state = {
            showSubMenu: false
        };
        this.showSubMenu = this.showSubMenu.bind(this);
    }

    showSubMenu(){
        this.setState({showSubMenu: !this.state.showSubMenu})
    }

    onTextChanged(){
        localStorage.removeItem(AUTH_TOKEN)
        if ("callbackParent" in this.props) {
            this.props.callbackParent();
        }
        this.props.history.push('/');
    }

    render(){
        const authToken = localStorage.getItem(AUTH_TOKEN);
        const submenu = this.state.showSubMenu;
        const iconSub = !submenu ? <ChevronDown size={14}/> : <ChevronUp size={14}/>;

        return(
            <div>
            {authToken ? (
                <div className="pr2-ns pv2 ph3 mv2-ns">
                    <div  className="tc grow-large hover-blue"
                          onClick={() => this.showSubMenu()}>
                        <User/>
                        <div className="dn db-ns">
                            <div className="dib"> Usuario</div>
                            <div className="dib v-mid"> { iconSub } </div>
                        </div>
                    </div>

                    { submenu ?
                    <div className="bg-white br3 mt3-ns mt2 absolute right-1" >
                        <div className="child pointer ph3 pv2 br4 db
                        bg-animate hover-bg-light-gray">
                            <div className="dib v-mid mr2"> <Settings size={16}/> </div>
                            <div className="dib"> Cambiar Contraseña </div>
                        </div>
                        <div className="child pointer ph3 pv2 br4 db
                        bg-animate hover-bg-light-gray"
                             onClick={() => this.onTextChanged()}>
                            <div className="dib v-mid mr2"><LogOut size={16} /></div>
                            <div className="dib"> Salir </div>
                        </div>
                    </div>
                    : null }
                </div>

            ) : (
                <Redirect to='/' />
            )}
            </div>
        )
    }
}

export default withRouter(Logout)
