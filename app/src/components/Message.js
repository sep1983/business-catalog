import React, { Component } from 'react'
import { AUTH_TOKEN } from '../constants'
import { Redirect } from 'react-router-dom'
import {CheckCircle, Info, AlertCircle, XCircle, XSquare} from 'react-feather';

const colorAlerts = {
    success  : ' b--green',
    info     : ' b--blue',
    warning  : ' b--gold',
    error    : ' b--red'
}

const iconAlerts = {
    success  : <CheckCircle/>,
    info     : <Info/>,
    warning  : <AlertCircle/>,
    error    : <XCircle/>
}

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allAlerts: []
        }
        this.closeSession = this.closeSession.bind(this);
        this.closeByUser = this.closeByUser.bind(this);
    }

    closeSession(){
        localStorage.removeItem(AUTH_TOKEN);
        return <Redirect to='/' />;
    }

    isEmpty(obj) {
        for(let key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.data !== this.props.data){
            //Perform some operation here
            let tmp_alerts = this.state.allAlerts;
            if(!this.isEmpty(this.props.data)){
                tmp_alerts.push(this.props.data);
                this.setState({allAlerts: tmp_alerts});
            }
        }
    }


    setTimer(dat){
        if(dat.delay == undefined){
            dat.delay = 3000;
        }
        setTimeout(function(dat){
            let tmp_alert = this.state.allAlerts;
            tmp_alert.splice(dat.id,1);
            this.setState({allAlerts: tmp_alert});
        }.bind(this, dat), dat.delay);
    }

    closeByUser(alertId){
        let tmp_alert = this.state.allAlerts;
        tmp_alert.splice(alertId,1);
        this.setState({allAlerts: tmp_alert});
    }

    render(){
        let elementsToShow;
        if (this.props.message !== undefined && this.props.message.search('Signature') !==  -1){
            this.closeSession();
        }

        return( <div className='fixed bottom-0 right-2 tr w-20-l w-40-m w-80'>
                {this.state.allAlerts.map((value, index) => {
                    this.setTimer({id:index, delay:value.delay});
                    return <Alert
                                key={index}
                                id={index}
                                click={this.closeByUser}
                                data={value}></Alert>
                })}
                </div>);
    }

}

class Alert extends Component {
    
    constructor(props) {
        super(props);
        this._handleDelete = this._handleDelete.bind(this);
    }

    _handleDelete(id){
        this.props.click(this.props.id);
    }

    render() {
        const colorMessage = ["relative mr4 mb2 bg-white dib pa2 ba  br2",
                              colorAlerts[this.props.data.color]].join(' ');
        const iconMessage  = iconAlerts[this.props.data.color];
        const textMessage  = this.props.data.text;
        const cer = <XSquare/>;

        return  <div className={colorMessage} role="alert">
                    <span
                        className='absolute top-0 right--2'
                        onClick={this._handleDelete}
                    >{cer}</span>
                    <div className='fl mr2 b fw9'>{iconMessage}</div>
                    <span className='fw5 mr4 black-80 b-ns'> {textMessage} </span>
                </div>
    }

}

export default Message
