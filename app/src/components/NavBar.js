import React, { Component } from 'react'
import {ArrowDown, ChevronDown, ChevronUp, Edit3, Plus, Save, Square, Trash, Trello} from "react-feather";
import Logout from "./Logout";

class NavBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            alert: {},
            showSubMenu: false
        };
        this.showSubMenu = this.showSubMenu.bind(this);
    }

    showSubMenu(e){
        e.preventDefault();
        this.setState({showSubMenu: !this.state.showSubMenu})
    }

    _handleMenu(id){
        this.props.click(id);
        this.setState({
            showSubMenu: false,
        });
    }

    render() {

        const { showSubMenu } = this.state;
        const iconSub = !showSubMenu ? <ChevronDown size={14}/> : <ChevronUp size={14}/>;

        return (
            <nav className="fixed top-0 db dt-l
                    shadow-3 bg-white-90
                    w-100 border-box pa1 ph4-l z-5">
                <div className="flex fl">
                    <div className="pr2-ns pv2 ph3 mv2-ns">
                        <div className="tc hover-blue grow-large"
                             onClick={(e) => this.showSubMenu(e)}>
                            <Trello/>
                            <div className="dn db-ns">
                                <div className="dib"> Categoria</div>
                                <div className="dib v-mid"> {iconSub} </div>
                            </div>
                        </div>
                        {showSubMenu ?
                            <div className="bg-white br3 mt3-ns mt2 absolute left-1">
                                <div className="child pointer ph3 pv2 br4 db
                                    bg-animate hover-bg-light-gray"
                                     onClick={() => this._handleMenu("showNewCat")}>
                                    <div className="dib v-mid mr2"><Plus size={16}/></div>
                                    <div className="dib"> Nueva</div>
                                </div>
                                <div className="child pointer ph3 pv2 br4 db
                                    bg-animate hover-bg-light-gray"
                                     onClick={() => this._handleMenu("showEdiCat")}>
                                    <div className="dib v-mid mr2"><Edit3 size={16}/></div>
                                    <div className="dib"> Editar</div>
                                </div>
                            </div>
                            : null}
                    </div>
                    <div className="hover-blue grow-large tc pr2-ns pv2 ph3 mv2-ns"
                         onClick={() => this._handleMenu("showItem")}>
                        <Square/>
                        <div className="dn db-ns"> Articulo</div>
                    </div>
                </div>
                <div className="fr">
                    <Logout/>
                </div>
            </nav>
        );
    }
}

export default NavBar
