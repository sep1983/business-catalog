import React, { Component } from 'react';
import { Mutation } from "react-apollo";
import { GET_CATEGORY, NEWCATEG } from '../queries/RepoCategoriesQuery';
import { Save } from 'react-feather';

class NewCateg extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nameCat: '',
            id_categ: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.validateForm = this.validateForm.bind(this);
    }

    handleChange(event) {
        this.setState({ nameCat: event.target.value}); 
    }

    validateForm(){
        if(this.state.nameCat && this.state.nameCat.length > 2){
            return false;
        }
        return true;
    }

    render() {
        const classtd = "pv2 pr3 bb b--black-20 measure";
        const classhd = "fw6 tl pa3 bg-white";        
        const { nameCat, id_categ, showMessage } = this.state;

        return (
            <div className="pa4 black-80">
                <legend className="f4 fw6 ph0 mh0">Agregar Categoria</legend>
                <div className="mt3">

                    <label htmlFor="name" className="f6 b db mb2">Nombre <span className="red">*</span></label>

                    <input
                        className="input-reset b--black-30 ba
                        bg-transparent pa2 mb2 db w-100 br4"
                        name="nameCat"
                        type="text"
                        placeholder=""
                        value={nameCat}
                        onChange={this.handleChange}
                        required/>

                    <Mutation
                        mutation={NEWCATEG}
                        variables={{ nameCat }}
                        onCompleted={()=> {
                            this.props.message({
                                text: 'Se creo el registro',
                                color:'success' });
                            }
                        }

                        onError={(data) => {
                            this.props.message({
                                text: 'Ne creo el registro, error_5237',
                                color:'error' });
                            }
                        }

                        refetchQueries={(color) => {
                            return [{ query:GET_CATEGORY, variables:{color: color}}];
                        }}
                    >
                        {(mutation) => {
                            const butt = this.validateForm() ? ' light-gray ' : '  bg-animate hover-bg-silver black ';
                            const classes = "child pointer ph3 pv2 br4 " + butt +
                                " bg-moon-gray";

                            return (
                                <div className="tc ma4">
                                    <a onClick  = {() => this.validateForm()?  (e) =>e.preventDefault() : mutation}
                                       className= {classes}>
                                        <div  className="dib v-mid">
                                            <div title="Guardar">
                                                <Save size={14}/>
                                            </div>
                                        </div>
                                        <div className="dib ml1"> Guardar </div>
                                    </a>
                                </div>
                            )
                        }}
                    </Mutation>

                </div>
            </div>
        )
    }
}

export default NewCateg
