import React, { Component } from 'react'
import { Save, Upload} from "react-feather";
import SelectCategory from "../CommonQuerys"
import { NEW_ITEM, LIST_ITEMS } from '../queries/ItemQuery';
import { AUTH_TOKEN } from '../constants';
import { Mutation } from 'react-apollo'
import {Redirect} from "react-router-dom";

class NewItem extends Component {

    constructor(props) {
      super(props);
      this.state = {
          category: '',
          title: '',
          price: 0,
          quantity: 0,
          description: '',
          fileLogo: null
      };
        this.handleChangeCategory = this.handleChangeCategory.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
        this.handleChangeDescription = this.handleChangeDescription.bind(this);
        this.handleImageItem = this.handleImageItem.bind(this);
    }

    handleImageItem(event){
        event.preventDefault();

        let reader = new FileReader();
        let file = event.target.files[0];

        reader.onloadend = function(event) {
            this.setState({
                fileLogo: event.target.result
            });
        }.bind(this);

        if (file.type === "image/png"){
            reader.readAsDataURL(file)
        }
    }

    validateForm(){
        if( this.state.category &&
            this.state.fileLogo
        ){
            return false;
        }
        return true;
    }

    handleChangeCategory(categoryId) {
        this.setState({ category: categoryId });
    }

    handleChangeTitle(event) {
        this.setState({title: event.target.value});
    }

    handleChangePrice(event) {
        this.setState({price: event.target.value});
    }

    handleChangeQuantity(event) {
        this.setState({quantity: event.target.value});
    }

    handleChangeDescription(event) {
        this.setState({description: event.target.value});
    }

    render() {

        const classin = "input-reset bg-transparent b--black-30 ba pa2 mb2 db br4 w-100";
        const classFlex = "mb1 pb2 w-100";

        const { category, title, price, quantity,
                description, fileLogo } = this.state;

        return (
            <div className={classFlex}>
                <div className='pl4 pb3 pt4'>
                    <legend className="f4 fw6 ph0 mh0">Nuevo Articulo</legend>
                </div>
                <div className="flex justify-around flex-wrap black-80">
                    <div className={classFlex}>
                        <div className='ml4 mr4'>
                            <label className="f6 b db mb2">Categoria <span className="red">*</span></label>
                            <SelectCategory changeSelect={this.handleChangeCategory}/>
                        </div>
                    </div>
                    <div className={classFlex}>
                        <div className='ml4 mr4'>
                            <label className="f6 b db mb2">Titulo</label>
                            <input
                                className={classin}
                                type="text"
                                value={title}
                                onChange={this.handleChangeTitle}/>
                        </div>
                    </div>
                    <div className={classFlex}>
                        <div className='ml4 mr4'>
                            <label className="f6 b db mb2">Precio</label>
                            <input
                                className={classin}
                                type="number"
                                value={price}
                                onChange={this.handleChangePrice}/>
                        </div>
                    </div>
                    <div className={classFlex}>
                        <div className='ml4 mr4'>
                            <label className="f6 b db mb2">Cantidad</label>
                            <input
                                className={classin}
                                type="number"
                                placeholder="Cantidad de articulos disponibles"
                                value={quantity}
                                onChange={this.handleChangeQuantity}/>
                            </div>
                    </div>
                    <div className={classFlex}>
                        <div className='ml4 mr4'>
                            <label className="f6 b db mb2">Descripción</label>
                            <textarea
                                className={classin}
                                rows="4"
                                name="description"
                                type="text"
                                placeholder="Descripcion del producto"
                                value={description}
                                onChange={this.handleChangeDescription}/>
                        </div>
                    </div>
                    <div className={classFlex}>
                        <div className='ml4 mr4'>
                            <div className='flex'>
                                <div className="flex w-40 mt2">
                                    <label className="f6 b db mb2">Subir  <span className="red">*</span> Imagen </label>
                                    <div className='relative items-center'>
                                        <input
                                            type="file"
                                            accept="image/png"
                                            className="o-0 w-100 relative z-1 h-100"
                                            onChange={this.handleImageItem}/>
                                        <div
                                            className="z-0 w-auto
                                            absolute ml2 top-0 left-0
                                            button-reset"
                                        ><Upload strokeWidth={2}/> </div>
                                    </div>
                                </div>
                                <div className="w-50">
                                    <div className='dib v-mid'>
                                        {fileLogo ?
                                            <img className="br4 pa1 ba ml2 b--black-10 h3 w3"
                                                src={fileLogo} target="_blank" />
                                        : '' }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='ma2 tr'>
                        <Mutation
                            mutation={NEW_ITEM}

                            variables={{categ: category,
                                        title: title,
                                        unitPrice: price,
                                        quantity: quantity,
                                        thumbnail: fileLogo,
                                        description: description }}

                            onCompleted={()=> {
                                this.setState({
                                        category: '',
                                        title: '',
                                        price: 0,
                                        quantity: 0,
                                        description: '',
                                        fileLogo: null
                                    });
                                this.props.message({
                                    text: 'Se creo el nuevo Articulo',
                                    color:'success' })
                            }}

                            onError={(data) => {
                                this.props.message({
                                    text: data.message + ' , error_5291',
                                    color:'error' });
                            }}

                            refetchQueries={() => {
                                return [{ query:LIST_ITEMS }];
                            }}
                        >
                            {(mutation, { loading, error }) => {
                                if (error){
                                    if (error.message.search('Signature') !==  -1){
                                        localStorage.removeItem(AUTH_TOKEN);
                                        return <Redirect to='/' />
                                    }
                                }

                                const butt = this.validateForm() ? ' light-gray ' : '  bg-animate hover-bg-silver black ';
                                const classes = "child pointer ph3 pv2 br4 db " + butt +
                                                " bg-moon-gray";

                                return (
                                    <a onClick  = {() => this.validateForm()? (e) =>e.preventDefault() : mutation}
                                         className= {classes}>
                                        <div  className="dib v-mid">
                                            <div title="Guardar">
                                                <Save size={14}/>
                                            </div>
                                        </div>
                                        <div className="dib ml1"> Guardar </div>
                                    </a>
                                )
                            }}
                        </Mutation>
                    </div>
                </div>
            </div>
        )
    }
}

export default NewItem
