import React, { Component } from 'react'
import { Query, Mutation } from 'react-apollo'
import { AUTH_TOKEN } from '../constants'
import { GET_CATEGORY } from '../queries/RepoCategoriesQuery';
import { DEL_ITEM, CHANGE_ITEM } from '../queries/ItemQuery';


class SelectCateg extends Component {
    constructor(props) {
        super(props);        
        this.state = {            
            category: this.props.valCatg
        }        
        this.handleChangeCategory = this.handleChangeCategory.bind(this);
    }
    
    handleChangeCategory(event) {
        this.setState({category: event.target.value});
    }
    
    render() {
        const {category } = this.state;
        return (
            <Query query={GET_CATEGORY}>
            {({ loading, error, data }) => {
                if (loading)
                    return <div>Fetching</div>
                if (error)
                    return <div>Error</div>

                const AllCategories = data.allCategories.edges;
                const classin = "ba b--black-20 pa2 mb2 db w-90";
                const color = AllCategories.map(function (c, index) {
                    return (<option key={index} value={c.node.id}>{c.node.name}</option>)
                });

                color.push(<option key='1000' value=''></option>);
                return (
                        <select
                            className={classin}
                            value={category}
                            onChange={this.handleChangeCategory}>                            
                            {color}
                        </select>
                )
            }}
            </Query>                
        )
    }

}


class RowEditItems extends Component {

    constructor(props) {
        super(props);        
        this.state = {
            id: this.props.iditem,
            idRow: this.props.iditemRow,
            inactive: this.props.inactive,
            category: this.props.idCateg,
            title: this.props.title,
            price: this.props.unitPrice ,
            quantity: this.props.quantity,
            description: this.props.descrip,
            fileLogo: this.props.file,
            remove: false
        };
        
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.handleChangeQuantity = this.handleChangeQuantity.bind(this);
        this.handleChangeDescription = this.handleChangeDescription.bind(this);
        this.handleChangeInactive = this.handleChangeInactive.bind(this);
        this.handleImage = this.handleImage.bind(this);
        this.trRowLine = this.trRowLine.bind(this);
    }

    handleImage(event) {
        event.preventDefault();

        let reader = new FileReader();
        let file = event.target.files[0];

        reader.onloadend = function (event) {
            this.setState({
                fileLogo: event.target.result
            });
        }.bind(this);

        if (file.type === "image/png") {
            reader.readAsDataURL(file)
        }
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    handleChangeTitle(event) {
        this.setState({title: event.target.value});
    }

    handleChangePrice(event) {
        this.setState({price: event.target.value});
    }

    handleChangeInactive(event) {
        this.setState({inactive: !this.state.inactive});
    }

    handleChangeQuantity(event) {
        this.setState({quantity: event.target.value});
    }

    handleChangeDescription(event) {
        this.setState({description: event.target.value});
    }
    
    trRowLine(){
        
        const classtd = "pv2 pr3 bb b--black-20 v-top  tc";
        const classhd = "fw6 tl pa3 bg-white";
        const classin = "input-reset ba b--black-20 pa2 mb2 db w-90";

        const { id, idRow, category, inactive, title, price, quantity,
                description, fileLogo } = this.state;
        
        return (<tr className="stripe-dark">           
                    <td className={classtd}>
                        {id}
                    </td>
                    
                    <td className={classtd}>
                        <input type="checkbox" 
                        checked={this.state.inactive}
                        onChange={this.handleChangeInactive}/>
                    </td>
                    
                    <td className={classtd}>
                        <SelectCateg
                        valCatg={this.state.category}
                        callChangeParent={this.handleChangeCategory}/>                                                    
                    </td>                    
                    
                    <td className={classtd}>
                        <input className={classin}
                        type="input"
                        onChange={this.handleChangeTitle}
                        value={this.state.title}/>
                    </td>
                    
                    <td className={classtd}>
                        <input className={classin}
                        type="number"
                        min="0"
                        onChange={this.handleChangePrice}
                        value={this.state.price}/>
                    </td>
                    
                    <td className={classtd}>
                        <input className={classin}
                        type="number"
                        min="0"
                        onChange={this.handleChangeQuantity}
                        value={this.state.quantity}/>
                    </td>
                    
                    <td className={classtd}>
                        <textarea rows="4" cols="10"
                        onChange={this.handleChangeDescription}
                        value={this.props.descrip}/>
                    </td>  
                   
                    <td className={classtd}>
                        <label className="custom-input tc">
                            <span className="lnr lnr-upload"></span> Imagen
                            <input
                                type="file"
                                accept="image/png"
                                className="inputfile"
                                onChange={this.handleImage}/>                        
                        </label>
                        
                        <div className="tc">
                        {fileLogo?
                            <img className="br-100 pa1 ba b--black-10 h3 w3"
                            src={fileLogo} target="_blank" />                        
                            : ''
                        }
                        </div>
                    </td>
                    
                    <td className={classtd}>
                        <Mutation
                            mutation={CHANGE_ITEM}
                            variables={{id: idRow,            
                                        categ: category,
                                        noActive: inactive,            
                                        title: title,
                                        unitPrice: price,
                                        quantity: quantity,
                                        thumbnail: fileLogo,
                                        description: description}}
                            onCompleted={() => {
                                    this.setState({showMessage: true});
                                }}
                            >
                            {(mutation, { loading, error }) => {

                                if (loading)
                                    return <p>Loading...</p>;

                                if (error) {
                                    if (error.message.search('Signature') !== -1) {
                                        localStorage.removeItem(AUTH_TOKEN);
                                    }
                                }

                                return  <div>
                                            <button
                                                onClick={mutation}>
                                                <span className="lnr lnr-inbox"></span>
                                            </button>
                                        </div>
                            }}
                        </Mutation>
                        <Mutation
                            mutation={DEL_ITEM}
                            variables={{id: idRow}}

                            onCompleted={() => {
                                    this.setState({showMessage: true,
                                                   remove: true});
                                } }      
                            >
                            
                            {(mutation, { loading, error }) => {

                                if (loading)
                                    return <p>Loading...</p>;

                                if (error) {
                                    if (error.message.search('Signature') !== -1) {
                                        localStorage.removeItem(AUTH_TOKEN);
                                                        }
                                }

                                return  <div>
                                            <div>
                                                <button
                                                    onClick={mutation}>
                                                    <span className="lnr lnr-trash"></span>
                                                </button>
                                            </div>
                                        </div>
                            }}
                        </Mutation>
                    </td>
                </tr>
        )
    }

    render() {
        return (this.state.remove ? '' : this.trRowLine())
    }
    
}

export default RowEditItems
