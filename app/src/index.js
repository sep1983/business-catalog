import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { setContext } from 'apollo-link-context'
import { BrowserRouter, Switch, Route} from 'react-router-dom'
import { AUTH_TOKEN } from './constants'
import EditContent from './components/EditContent'

const httpLink = createHttpLink({
  uri: 'http://localhost:5000/graphql',
  credentials: 'same-origin'
})


const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem(AUTH_TOKEN)
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  }
})

const client = new ApolloClient({
  cache: new InMemoryCache(),
  ssrMode: true,
  connectToDevTools: true,
  link: authLink.concat(httpLink)
})

ReactDOM.render(
  <BrowserRouter>
    <ApolloProvider client={client}>
        <Switch>
            <Route exact path="/" component={App} />
            <Route exact path="/list" component={EditContent} />
        </Switch>
    </ApolloProvider>
  </BrowserRouter>,
  document.getElementById('root')
)
