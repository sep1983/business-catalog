import gql from "graphql-tag";

const NEW_ITEM = gql`
    mutation CreateItem($categ: String!,
                        $title: String!,
                        $unitPrice: Int,
                        $quantity: Int,
                        $thumbnail: String!,
                        $description: String!){
        createItem(categ:$categ,
                   title:$title,
                   unitPrice:$unitPrice,
                   quantity:$quantity,
                   thumbnail:$thumbnail,
                   description:$description){
            ok
        }
    }`;


const CHANGE_ITEM = gql`
    mutation changeItem($id: String!
                        $categ: String!,
                        $noActive: Boolean!,
                        $title: String!,
                        $unitPrice: Int,
                        $quantity: Int,
                        $thumbnail: String!,
                        $description: String){
        changeItem(id:$id,
                   categ:$categ,
                   noActive: $noActive,
                   title:$title,
                   unitPrice:$unitPrice,
                   quantity:$quantity,
                   thumbnail:$thumbnail,
                   description:$description){
            ok
        }
    }`;


const DEL_ITEM = gql`
    mutation DeleteItem($id: String!){
        deleteItem(id: $id) {
            ok
        }    
    }`;


const LIST_ITEMS = gql`
{
    allItems {
      edges {
        node {
          id       
          title
          unitPrice
          quantity
          noActive
          thumbnail
          description          
          category{
            id
            name
          }
        }
      }
    } 
}`;


export { NEW_ITEM, LIST_ITEMS, DEL_ITEM, CHANGE_ITEM };
