import gql from "graphql-tag";

const GET_CATEGORY = gql`
    {
      allCategories{
        edges{
          node{
            name
            id
          }
        }
      }
    }`;

const NEWCATEG = gql`
    mutation CreateCategory($nameCat: String!){
        createCategory(name: $nameCat){
            categ{
                name
                id
            }
        }
    }
`;

const CHANGECATEG = gql`
    mutation changeCategory($name: String!, $id: String!){
        changeCategory(name: $name, id: $id){
            item{
                name
                id
            }
        }
    }
`;

const DELETECATEG = gql`
    mutation deleteCategory($id: String!){
        deleteCategory(id: $id){
            ok
            message
            item{
                name
                id
            }
        }
    }
`;

export { GET_CATEGORY as default,
    NEWCATEG,
    GET_CATEGORY,
    CHANGECATEG,
    DELETECATEG};
