import gql from "graphql-tag";

const LOGIN = gql`
    mutation LoginMutation($email: String!, $passwd: String!) {
        login(email: $email, passwd: $passwd) {
            authToken
        }
    }
`;

const CREATEUSER = gql`
    mutation createUser($name: String!, $passwd: String!, $email: String!){
        createUser(name: $name, passwd: $passwd, email: $email){
            token
        }
    }
`;

export {LOGIN as default, CREATEUSER };
