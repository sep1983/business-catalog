#! usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask
from flask_graphql import GraphQLView
from flask_cors import CORS

# https://github.com/teknokeras/flask-graphql-template
# https://medium.com/@n.raj.suthar/building-a-mini-blogger-with-graphene-and-react-hooks-api-1deb06cf2d47

app = Flask(__name__)
CORS(app)

app.debug = True
app.config["SQLALCHEMY_ECHO"] = True
# Setup the Flask-JWT-Extended extension
app.config['SECRET_KEY'] = 'Una clave del servidor unica'  # Change this!


@app.route('/')
def index():
    return "Go to /graphql"


if __name__ == "__main__":
    from schema import schema

    app.add_url_rule('/graphql',
                     view_func=GraphQLView.as_view('graphql',
                                                   schema=schema,
                                                   graphiql=True))

    app.run()
