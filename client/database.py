#! usr/bin/python
# -*- coding: utf-8 -*-

from db import Base
from app import app
from datetime import datetime, timedelta
from sqlalchemy import create_engine, func
from sqlalchemy.orm import column_property, backref, relationship
from werkzeug.security import generate_password_hash, check_password_hash
import jwt

from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text, Boolean, String


def oneyearmore():
    dt = datetime.utcnow()
    return dt.replace(year= dt.year + 1)


class User(Base):
    __tablename__ = 'user'
    id = Column('id_user', Integer, primary_key=True)
    expires = Column(DateTime, default=oneyearmore())
    created = Column(DateTime, default=datetime.utcnow)
    name = Column(String(200), unique=True )
    no_active = Column(Boolean, default=True)
    passwd = Column(String(128))
    email = Column(String(150), unique=True)

    def set_password(self, password):
        self.passwd = generate_password_hash(password)

    def get_id(self):
        return self.id

    def check_password(self, password):
        return check_password_hash(self.passwd, password)

    def encode_auth_token(self, user_id):
        try:
            payload = {
                'exp': datetime.utcnow() + timedelta(days=0, seconds=1800),
                'iat': datetime.utcnow(),
                'sub': user_id
            }

            return jwt.encode(
                payload,
                app.config.get('SECRET_KEY'),
                algorithm='HS256'
            ).decode()

        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(request):
        auth_header = request.headers.environ.get('HTTP_AUTHORIZATION')
        if auth_header:
            auth_token = auth_header.split(" ")[1]
        else:
            auth_token = ''
        try:
            payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'))
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

    def __repr__(self):
        return '<User %r %r>' % self.name, self.id


class LogSys(Base):
    __tablename__ = 'log_sys'
    id = Column( "id_log_sys", Integer, primary_key=True )
    sys_action = Column ( String(200), nullable=False )
    details = Column ( String(200) )
    created = Column ( DateTime, default=datetime.utcnow )
    id_user_createby = Column( Integer, ForeignKey( "user.id_user" ), nullable=False )

    user = relationship( User, primaryjoin= id_user_createby == User.id, \
                        cascade="all, delete, delete-orphan", \
                        single_parent=True)


class Category(Base):
    __tablename__ = 'category'
    id = Column("id_category", Integer, primary_key=True)
    name = Column(String(200), unique=True)


class Item(Base):
    __tablename__ = 'item'
    id = Column("id_item", Integer, primary_key=True)
    id_user_createby = Column(Integer, ForeignKey("user.id_user"), nullable=False)
    id_category = Column(Integer, ForeignKey("category.id_category"), nullable=False)
    title = Column(String(300), unique=True )
    created = Column(DateTime, default=datetime.utcnow)
    unit_price = Column(Integer, default=0)
    quantity = Column(Integer, default=0)
    no_active = Column(Boolean, default=False)
    thumbnail = Column(Text, default='')
    description = Column(Text)

    user = relationship( User, primaryjoin= id_user_createby == User.id, \
                        cascade="all, delete, delete-orphan", \
                        single_parent=True)


    category = relationship("Category", primaryjoin= id_category == Category.id, \
                              cascade="all, delete, delete-orphan", \
                              single_parent=True)


class ItemImage(Base):
    __tablename__ = 'item_image'
    id = Column("id_item", Integer, ForeignKey("item.id_item"), primary_key=True, nullable=False)
    image = Column(Text, unique=True)

    item = relationship( Item, primaryjoin= id == Item.id, \
                        cascade="all, delete, delete-orphan", \
                        single_parent=True)

