from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session,  sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Replace 'sqlite:///rfg.db' with your path to database
engine = create_engine('sqlite:///cata_db.db', convert_unicode=True, connect_args={'check_same_thread': False})
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()
