#! usr/bin/python
# -*- coding: utf-8 -*-
import graphene
import base64
import copy
from db import db_session
from graphql import GraphQLError
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from database import User as UserModel
from database import Item as ItemModel
from database import LogSys as LogSysModel
from database import Category as CategoryModel


def decode_id(strtochange):
    return base64.b64decode(strtochange).decode().split(':')[1]


def send_commit(model):
    if model:
        db_session.add(model)
    try:
        db_session.commit()
        complete = True
    except Exception as e:
        db_session().rollback()
        db_session.flush()
        complete = False
        print(e)
    return complete


def require_auth(method):
    def wrapper(self, *args, **kwargs):
        auth_resp = UserModel.decode_auth_token(args[2])
        if not isinstance(auth_resp, str):
            kwargs['user'] = UserModel.query.filter_by(id=auth_resp).first()
            return method(self, *args, **kwargs)
        raise GraphQLError(auth_resp)

    return wrapper


class Users(SQLAlchemyObjectType):
    class Meta:
        model = UserModel
        interfaces = (relay.Node,)
        exclude_fields = ('passwd')


class UsersConnection(relay.Connection):
    class Meta:
        node = Users


# Used to Create New User
class CreateUser(graphene.Mutation):
    class Input:
        name = graphene.String(required=True)
        passwd = graphene.String(required=True)
        email = graphene.String(required=True)

    user = graphene.Field(Users)
    token = graphene.String()
    ok = graphene.Boolean()

    @classmethod
    def mutate(cls, _, args, context, info):
        user = UserModel(name=args.get('name'), email=args.get('email'))
        user.set_password(args.get('passwd'))
        complete = send_commit(user)
        auth_token = user.encode_auth_token(user.id)
        return CreateUser(user=user, token=auth_token, ok=complete)


# Used to Change Username with Email
class changeUsername(graphene.Mutation):
    class Input:
        username = graphene.String()
        email = graphene.String()

    ok = graphene.Boolean()
    user = graphene.Field(Users)

    @classmethod
    @require_auth
    def mutate(cls, *args, **kwargs):
        email = args[1].get('email')
        username = args[1].get('username')
        user = UserModel.query.filter_by(email=email, id=kwargs['user']).first()
        user.name = username
        complete = send_commit(user)
        return changeUsername(user=user, ok=complete)


class Categories(SQLAlchemyObjectType):
    class Meta:
        model = CategoryModel
        interfaces = (relay.Node,)


class CategoriesConnection(relay.Connection):
    class Meta:
        node = Categories


# Used to Create Category
class createCategory(graphene.Mutation):
    class Input:
        name = graphene.String()

    ok = graphene.Boolean()
    categ = graphene.Field(Categories)

    @classmethod
    @require_auth
    def mutate(cls, *args, **kwargs):
        categ_save = CategoryModel(name=args[1].get('name'))
        complete = send_commit(categ_save)
        return createCategory(categ=categ_save, ok=complete)


class Items(SQLAlchemyObjectType):
    class Meta:
        model = ItemModel
        interfaces = (relay.Node,)


class ItemsConnection(relay.Connection):
    class Meta:
        node = Items


# Used to Create Item
class CreateItem(graphene.Mutation):
    class Input:
        categ = graphene.String(required=True)
        title = graphene.String(required=True)
        unit_price = graphene.Int()
        quantity = graphene.Int()
        thumbnail = graphene.String()
        description = graphene.String(required=True)

    ok = graphene.Boolean()
    itemsM = graphene.Field(Items)

    @classmethod
    @require_auth
    def mutate(cls, *args, **kwargs):
        itemM = ItemModel(id_user_createby=kwargs['user'].id,
                          id_category=decode_id(args[1].get('categ')),
                          title=args[1].get('title'),
                          unit_price=args[1].get('unit_price'),
                          quantity=args[1].get('quantity'),
                          thumbnail=args[1].get('thumbnail'),
                          description=args[1].get('description'))

        complete = send_commit(itemM)
        return CreateItem(itemsM=itemM, ok=complete)


# Used to Create Item
class DeleteItem(graphene.Mutation):
    class Input:
        id = graphene.String(required=True)

    ok = graphene.Boolean()
    itemsM = graphene.Field(Items)

    @classmethod
    @require_auth
    def mutate(cls, *args, **kwargs):
        id_item = str(decode_id(args[1].get('id')))
        item_change = ItemModel.query.filter(
            ItemModel.id_user_createby == kwargs['user'].id,
            ItemModel.id == id_item).delete()

        complete = db_session.commit()
        return ChangeItem(item=item_change, ok=complete)


# Used to Change Category with id
class ChangeCategory(graphene.Mutation):
    class Input:
        id = graphene.String(required=True)
        name = graphene.String(required=True)

    ok = graphene.Boolean()
    item = graphene.Field(Categories)

    @classmethod
    @require_auth
    def mutate(cls, *args, **kwargs):
        id_item = str(decode_id(args[1].get('id')))
        categ_save = CategoryModel.query.filter(CategoryModel.id == id_item).first()

        if categ_save is not None:
            if args[1].get('name') is not None:
                categ_save.name = args[1].get('name').capitalize()

            complete = send_commit(categ_save)
            return ChangeCategory(item=categ_save, ok=complete)

        else:
            return ChangeCategory(item=None, ok=False)


# Used to Delete Category with id
class DeleteCategory(graphene.Mutation):
    class Input:
        id = graphene.String(required=True)

    ok = graphene.Boolean()
    item = graphene.Field(Categories)
    message = graphene.String()

    @classmethod
    @require_auth
    def mutate(cls, *args, **kwargs):
        id_item = str(decode_id(args[1].get('id')))
        categ = CategoryModel.query.filter(CategoryModel.id == id_item)

        q = db_session.query( CategoryModel, ItemModel).filter(
            CategoryModel.id == id_item
        ).filter(
            CategoryModel.id == ItemModel.id_category
        ).all()

        if q:
            return DeleteCategory(item=None, ok=False, message='Categoria con items relacionados, no se puede borrar.')

        if categ is not None:
            categ.delete()
            complete = db_session.commit()
            return DeleteCategory(ok=True)

        else:
            return DeleteCategory(item=None,
                                  ok=False,
                                  message='No se borro el registro')


# Used to Change Username with Email
class ChangeItem(graphene.Mutation):
    class Input:
        id = graphene.String(required=True)
        categ = graphene.String()
        title = graphene.String()
        unit_price = graphene.Int()
        quantity = graphene.Int()
        no_active = graphene.Boolean()
        thumbnail = graphene.String()
        description = graphene.String()

    ok = graphene.Boolean()
    item = graphene.Field(Items)

    @classmethod
    @require_auth
    def mutate(cls, *args, **kwargs):
        id_item = str(decode_id(args[1].get('id')))
        item_change = ItemModel.query.filter(
                        ItemModel.id_user_createby == kwargs['user'].id,
                        ItemModel.id == id_item).first()

        if item_change is not None:
            if args[1].get('categ') is not None:
                item_change.id_category = decode_id(args[1].get('categ'))
            if args[1].get('title') is not None:
                item_change.title = args[1].get('title')
            if args[1].get('unit_price') is not None:
                item_change.unit_price = args[1].get('unit_price')
            if args[1].get('quantity') is not None:
                item_change.quantity = args[1].get('quantity')
            if args[1].get('no_active') is not None:
                item_change.no_active = args[1].get('no_active')
            if args[1].get('thumbnail') is not None:
                item_change.thumbnail = args[1].get('thumbnail')
            if args[1].get('description') is not None:
                item_change.description = args[1].get('description')

            complete = send_commit(item_change)
            return ChangeItem(item=item_change, ok=complete)
        else:
            return ChangeItem(item=None, ok=False)


class SysAction(SQLAlchemyObjectType):
    class Meta:
        model = LogSysModel
        interfaces = (relay.Node,)


class SysActionConnection(relay.Connection):
    class Meta:
        node = Items


class loginUser(graphene.Mutation):
    class Input:
        email = graphene.String(required=True)
        passwd = graphene.String(required=True)

    user = graphene.Field(Users)
    auth_token = graphene.String()

    @classmethod
    def mutate(cls, _, args, context, info):
        user = UserModel.query.filter_by(email=args.get('email')).first()

        if user is None or not user.check_password(args.get('passwd')):
            raise GraphQLError("Invalid Credentials")

        return loginUser(user=user, auth_token=user.encode_auth_token(user.id))


class Viewer(graphene.ObjectType):
    class Meta:
        interfaces = (relay.Node,)

    all_users = SQLAlchemyConnectionField(UsersConnection)
    find_user = graphene.Field(lambda: Users, username=graphene.String())
    find_user_id = graphene.Field(lambda: Users, id=graphene.String())
    all_logs = SQLAlchemyConnectionField(SysActionConnection)


class Query(graphene.ObjectType):
    node = relay.Node.Field()

    viewer = graphene.Field(lambda: Viewer)
    find_category = graphene.Field(lambda: Categories, name=graphene.String())
    find_item = graphene.Field(lambda: Items, id=graphene.Int())
    all_categories = SQLAlchemyConnectionField(CategoriesConnection)
    all_items = SQLAlchemyConnectionField(ItemsConnection)

    @staticmethod
    def resolve_viewer(self, args, context, info):
        auth_resp = UserModel.decode_auth_token(context)
        if not isinstance(auth_resp, str):
            return Viewer()
        raise GraphQLError(auth_resp)

    @staticmethod
    def resolve_find_user(self, args, context, info):
        query = Users.get_query(context)
        username = args.get('username')
        # you can also use and_ with filter() eg: filter(and_(param1, param2)).first()
        return query.filter(UserModel.name == username).first()

    @staticmethod
    def resolve_find_user_id(self, args, context, info):
        query = Users.get_query(context)
        user_id = decode_id(args.get('id'))
        # you can also use and_ with filter() eg: filter(and_(param1, param2)).first()
        return query.filter(UserModel.id == user_id).first()


class MyMutations(graphene.ObjectType):
    create_user = CreateUser.Field()
    create_category = createCategory.Field()
    create_Item = CreateItem.Field()
    change_username = changeUsername.Field()
    change_category = ChangeCategory.Field()
    delete_item = DeleteItem.Field()
    delete_category = DeleteCategory.Field()
    login = loginUser.Field()
    change_Item = ChangeItem.Field()


schema = graphene.Schema(query=Query, mutation=MyMutations)
