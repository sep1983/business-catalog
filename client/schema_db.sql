BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `user` (
	`id_user`	INTEGER NOT NULL,
	`expires`	TEXT NOT NULL,
	`created`	TEXT NOT NULL,
	`name`	TEXT NOT NULL,
	`no_active`	INTEGER NOT NULL,
	`passwd`	TEXT NOT NULL,
	`email`	NVARCHAR ( 90 ) UNIQUE,
	PRIMARY KEY(`id_user`)
);
CREATE TABLE IF NOT EXISTS `sys_action` (
	`id_sys_action`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	NVARCHAR ( 120 )
);
CREATE TABLE IF NOT EXISTS `log_sys` (
	`id_log_sys`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`id_sys_action`	INTEGER NOT NULL,
	`details`	NVARCHAR ( 200 ),
	`created`	DATETIME NOT NULL,
	`id_user_createby`	INTEGER NOT NULL,
	FOREIGN KEY(`id_user_createby`) REFERENCES `user`(`id_user`)
);
CREATE TABLE IF NOT EXISTS `item_image` (
	`id_item`	INTEGER NOT NULL,
	`image`	BLOB UNIQUE,
	FOREIGN KEY(`id_item`) REFERENCES `item`(`id_item`) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT `pk_image_item` PRIMARY KEY(`id_item`,`image`)
);
CREATE TABLE IF NOT EXISTS `item` (
	`id_item`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`id_user_createby`	INTEGER NOT NULL,
	`id_category`	INTEGER NOT NULL,
	`title`	NVARCHAR ( 300 ) NOT NULL UNIQUE,
	`created`	DATETIME NOT NULL,
	`unit_price`	NUMERIC ( 10 , 2 ) NOT NULL,
	`quantity`	INTEGER NOT NULL,
	`no_active`	INTEGER NOT NULL,
	`thumbnail`	BLOB,
	`description`	NVARCHAR ( 300 ),
	FOREIGN KEY(`id_user_createby`) REFERENCES `user`(`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY(`id_category`) REFERENCES `category`(`id_category`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE TABLE IF NOT EXISTS `invoice_item` (
	`id_invoice_item`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`id_invoice`	INTEGER NOT NULL,
	`id_track`	INTEGER NOT NULL,
	`unit_price`	NUMERIC ( 10 , 2 ) NOT NULL,
	`quantity`	INTEGER NOT NULL,
	FOREIGN KEY(`id_invoice`) REFERENCES `invoice`(`id_invoice`) ON DELETE NO ACTION ON UPDATE NO ACTION,
	FOREIGN KEY(`id_track`) REFERENCES `track`(`id_track`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE TABLE IF NOT EXISTS `invoice` (
	`id_invoice`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`id_customer`	INTEGER NOT NULL,
	`invoice_date`	DATETIME NOT NULL,
	`billing_address`	NVARCHAR ( 70 ),
	`billing_city`	NVARCHAR ( 40 ),
	`billing_state`	NVARCHAR ( 40 ),
	`billing_country`	NVARCHAR ( 40 ),
	`billing_other`	NVARCHAR ( 200 ),
	`total`	NUMERIC ( 10 , 2 ) NOT NULL,
	FOREIGN KEY(`id_customer`) REFERENCES `customer`(`id_customer`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE TABLE IF NOT EXISTS `customer` (
	`id_customer`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`first_name`	NVARCHAR ( 40 ) NOT NULL,
	`last_name`	NVARCHAR ( 20 ) NOT NULL,
	`company`	NVARCHAR ( 80 ),
	`address`	NVARCHAR ( 70 ),
	`city`	NVARCHAR ( 40 ),
	`State`	NVARCHAR ( 40 ),
	`country`	NVARCHAR ( 40 ),
	`phone`	NVARCHAR ( 24 ),
	`Email`	NVARCHAR ( 60 ) NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS `category` (
	`id_category`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	NVARCHAR ( 120 ) UNIQUE
);
CREATE INDEX IF NOT EXISTS `IFK_user` ON `user` (
	`id_user`
);
CREATE INDEX IF NOT EXISTS `IFK_item_image` ON `item_image` (
	`id_item`
);
CREATE INDEX IF NOT EXISTS `IFK_item` ON `item` (
	`id_item`
);
COMMIT;
